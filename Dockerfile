FROM --platform=linux/amd64 dunglas/frankenphp:static-builder AS builder

# Copy your app
WORKDIR /go/src/app/dist/app

COPY . .

RUN composer install --ignore-platform-reqs --no-dev -a

# Build the static binary, be sure to select only the PHP extensions you want
WORKDIR /go/src/app/
RUN EMBED=dist/app/ \
    ./build-static.sh

FROM alpine:latest AS final

WORKDIR /app

COPY --from=builder /go/src/app/dist/frankenphp-linux-x86_64 myblog

ENTRYPOINT ["/app/myblog"]

CMD []
